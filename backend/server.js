const {request,response}=require('express')
const express=require('express')
const app=express()
const cors=require('cors')
app.use(express.json())
app.use(cors('*'))
const routerbook=require('./routes/book')
app.use('/book',routerbook)
app.get('/',(request,response)=>
{
    response.send('welcome to back end ')
})
app.listen(4000,'0.0.0.0',()=>
{
    console.log("port started on 4000")
})