const {request,response}=require('express')
const express =require('express')
const router=express.Router()
const db=require('../db')
const utils=require('../utils')

//book_id, book_title, publisher_name, author_name
// 1. GET  --> Display Book using the name from Containerized MySQL
router.get('/all',(request,response)=>
{
    const connection=db.openConnection()
    const statement=`select * from book`
    connection.query(statement,(error,data)=>
    {
        connection.end()
        if(error)
        {
            response.send(utils.createResult(error))
        }
        else{
            response.send(utils.createResult(error,data))
        }
    })
})

//2. POST --> ADD Book data into Containerized MySQL table

router.post('/add',(request,response)=>
{
    const connection=db.openConnection()
    const{book_id,book_title,publisher_name,author_name}=request.body
    const statement=`insert into book (book_id,book_title,publisher_name,author_name)
    values('${book_id}','${book_title}','${publisher_name}','${author_name}')`
    connection.query(statement,(error,data)=>
    {
        connection.end()
        if(error)
        {
            response.send(utils.createResult(error))
        }
        else{
            response.send(utils.createResult(error,data))
        }
    })
})
//3. UPDATE --> Update publisher_name and Author_name into Containerized MySQL table
router.patch('/update',(request,response)=>
{
    const connection=db.openConnection()
    const{publisher_name,book_id}=request.body
    const statement=`update book set publisher_name='${publisher_name}'  where book_id='${book_id}`
    connection.query(statement,(error,data)=>
    {
        connection.end()
        if(error)
        {
            response.send(utils.createResult(error))
        }
        else{
            response.send(utils.createResult(error,data))
        }
    })
})


//4. DELETE --> Delete Book from Containerized MySQL
router.delete('/delete',(request,response)=>
{
    const connection=db.openConnection()
    const{book_id}=request.body
    const statement=`delete from book where book_id=${book_id}`
    connection.query(statement,(error,data)=>
    {
        connection.end()
        if(error)
        {
            response.send(utils.createResult(error))
        }
        else{
            response.send(utils.createResult(error,data))
        }
    })
})

module.exports=router;